module.exports = function (grunt) {
  grunt.initConfig({
    ts: {
      default: {
        src: ["**/*.ts", "./src/*.ts", "!node_modules/**/*.ts"],
        verbose: true
      }
    },
    ts_clean: {
      build: {
        options: {
          // set to true to print files
          verbose: false
        },
        src: ['./dist/**/*'],
        dot: true
      }
    }
  });
  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks('grunt-ts-clean');
  grunt.registerTask("default", ["ts"]);
};