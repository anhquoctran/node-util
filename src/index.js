"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VERSION = '0.0.2';
var TypeChecker;
(function (TypeChecker) {
    function isBoolean(value) {
        return typeof value === 'boolean';
    }
    TypeChecker.isBoolean = isBoolean;
    function isObject(value) {
        return typeof value === 'object';
    }
    TypeChecker.isObject = isObject;
    function isFunction(value) {
        return typeof value === 'function';
    }
    TypeChecker.isFunction = isFunction;
    function isNumber(value) {
        return typeof value === 'number';
    }
    TypeChecker.isNumber = isNumber;
    function isUndefined(value) {
        return typeof value === 'number';
    }
    TypeChecker.isUndefined = isUndefined;
    function isNull(value) {
        return typeof value === null;
    }
    TypeChecker.isNull = isNull;
    function isSymbol(value) {
        return typeof value === 'symbol';
    }
    TypeChecker.isSymbol = isSymbol;
    function isIterable(iterable) {
        return iterable && typeof iterable[Symbol.iterator] === 'function';
    }
    TypeChecker.isIterable = isIterable;
    function isString(value) {
        return typeof value === 'string';
    }
    TypeChecker.isString = isString;
})(TypeChecker = exports.TypeChecker || (exports.TypeChecker = {}));
var String = (function () {
    function String() {
    }
    String.isNullOrWhiteSpace = function (value) {
        try {
            if (value == null || value == 'undefined')
                return true;
            return value.toString().replace(/\s/g, '').length < 1;
        }
        catch (e) {
            console.log(e);
            return false;
        }
    };
    String.join = function (delimiter) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        try {
            var firstArg = args[0];
            if (Array.isArray(firstArg) || firstArg instanceof Array) {
                var tempString = String.empty;
                var count = 0;
                for (var i = 0; i < firstArg.length; i++) {
                    var current = firstArg[i];
                    if (i < firstArg.length - 1)
                        tempString += current + delimiter;
                    else
                        tempString += current;
                }
                return tempString;
            }
            else if (typeof firstArg === 'object') {
                var tempString_1 = String.empty;
                var objectArg_1 = firstArg;
                var keys = Object.keys(firstArg);
                keys.forEach(function (element) {
                    tempString_1 += objectArg_1[element] + delimiter;
                });
                tempString_1 = tempString_1.slice(0, tempString_1.length - delimiter.length);
                return tempString_1;
            }
            var stringArray = args;
            return String.join.apply(String, __spreadArrays([delimiter], stringArray));
        }
        catch (e) {
            console.log(e);
            return String.empty;
        }
    };
    String.format = function (format) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        try {
            if (format.match(String.regexNumber))
                return String._format(String.regexNumber, format, args);
            if (format.match(String.regexObject))
                return String._format(String.regexObject, format, args, true);
            return format;
        }
        catch (e) {
            console.log(e);
            return String.empty;
        }
    };
    String._format = function (regex, format, args, parseByObject) {
        if (parseByObject === void 0) { parseByObject = false; }
        return format.replace(regex, function (match, x) {
            var s = match.split(':');
            if (s.length > 1) {
                x = s[0].replace('{', '');
                match = s[1].replace('}', '');
            }
            var arg;
            if (parseByObject)
                arg = args[0][x];
            else
                arg = args[x];
            if (arg == null || arg == undefined || match.match(/{\d+}/))
                return arg;
            arg = String.parsePattern(match, arg);
            return typeof arg != 'undefined' && arg != null ? arg : String.empty;
        });
    };
    String.parsePattern = function (match, arg) {
        switch (match) {
            case 'L':
                arg = arg.toLowerCase();
                return arg;
            case 'U':
                arg = arg.toUpperCase();
                return arg;
            case 'd':
                if (typeof arg === 'string') {
                    return String.getDisplayDateFromString(arg);
                }
                else if (arg instanceof Date) {
                    return String.format('{0:00}.{1:00}.{2:0000}', arg.getDate(), arg.getMonth(), arg.getFullYear());
                }
                break;
            case 's':
                if (typeof arg === 'string') {
                    return String.getSortableDateFromString(arg);
                }
                else if (arg instanceof Date) {
                    return String.format('{0:0000}-{1:00}-{2:00}', arg.getFullYear(), arg.getMonth(), arg.getDate());
                }
                break;
            case 'n':
                if (typeof arg !== 'string')
                    arg = arg.toString();
                var replacedString = arg.replace(/,/g, '.');
                if (isNaN(parseFloat(replacedString)) || replacedString.length <= 3)
                    break;
                var numberparts = replacedString.split(/[^0-9]+/g);
                var parts = numberparts;
                if (numberparts.length > 1) {
                    parts = [
                        String.join.apply(String, __spreadArrays([''], numberparts.splice(0, numberparts.length - 1))),
                        numberparts[numberparts.length - 1]
                    ];
                }
                var integer = parts[0];
                var mod = integer.length % 3;
                var output = mod > 0 ? integer.substring(0, mod) : String.empty;
                var firstGroup = output;
                var remainingGroups = integer.substring(mod).match(/.{3}/g);
                output = output + '.' + String.join('.', remainingGroups);
                arg = output + (parts.length > 1 ? ',' + parts[1] : '');
                return arg;
            default:
                break;
        }
        if ((typeof arg === 'number' || !isNaN(arg)) &&
            !isNaN(+match) &&
            !String.isNullOrWhiteSpace(arg))
            return String.formatNumber(arg, match);
        return arg;
    };
    String.getDisplayDateFromString = function (input) {
        var splitted;
        splitted = input.split('-');
        if (splitted.length <= 1)
            return input;
        var day = splitted[splitted.length - 1];
        var month = splitted[splitted.length - 2];
        var year = splitted[splitted.length - 3];
        day = day.split('T')[0];
        day = day.split(' ')[0];
        return day + "." + month + "." + year;
    };
    String.getSortableDateFromString = function (input) {
        var splitted = input.replace(',', '').split('.');
        if (splitted.length <= 1)
            return input;
        var times = splitted[splitted.length - 1].split(' ');
        var time = String.empty;
        if (times.length > 1)
            time = times[times.length - 1];
        var year = splitted[splitted.length - 1].split(' ')[0];
        var month = splitted[splitted.length - 2];
        var day = splitted[splitted.length - 3];
        var result = year + "-" + month + "-" + day;
        if (!String.isNullOrWhiteSpace(time) && time.length > 1)
            result += "T" + time;
        else
            result += 'T00:00:00';
        return result;
    };
    String.formatNumber = function (input, formatTemplate) {
        var count = formatTemplate.length;
        var stringValue = input.toString();
        if (count <= stringValue.length)
            return stringValue;
        var remainingCount = count - stringValue.length;
        remainingCount += 1;
        return new Array(remainingCount).join('0') + stringValue;
    };
    String._join = function (delimiter) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var temp = String.empty;
        for (var i = 0; i < args.length; i++) {
            if ((typeof args[i] == 'string' && String.isNullOrWhiteSpace(args[i])) ||
                (typeof args[i] != 'number' && typeof args[i] != 'string'))
                continue;
            var arg = '' + args[i];
            temp += arg;
            for (var i2 = i + 1; i2 < args.length; i2++) {
                if (String.isNullOrWhiteSpace(args[i2]))
                    continue;
                temp += delimiter;
                i = i2 - 1;
                break;
            }
        }
        return temp;
    };
    String.regexNumber = /{(\d+(:\w*)?)}/g;
    String.regexObject = /{(\w+(:\w*)?)}/g;
    String.empty = '';
    return String;
}());
exports.String = String;
var StringBuilder = (function () {
    function StringBuilder(value) {
        if (value === void 0) { value = String.empty; }
        this.values = [];
        this.values = new Array(value);
    }
    StringBuilder.prototype.toString = function () {
        return this.values.join('');
    };
    StringBuilder.prototype.append = function (value) {
        this.values.push(value);
    };
    StringBuilder.prototype.appendFormat = function (format) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.values.push(String.format.apply(String, __spreadArrays([format], args)));
    };
    StringBuilder.prototype.clear = function () {
        this.values = [];
    };
    return StringBuilder;
}());
exports.StringBuilder = StringBuilder;
//# sourceMappingURL=index.js.map