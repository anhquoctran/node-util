/**
 * Current version of this library
 */
export const VERSION: string = '0.0.2';

/**
 * Type checker
 */
export namespace TypeChecker {
  export function isBoolean(value: any) {
    return typeof value === 'boolean';
  }

  export function isObject(value: any) {
    return typeof value === 'object';
  }

  export function isFunction(value: any) {
    return typeof value === 'function';
  }

  export function isNumber(value: any) {
    return typeof value === 'number';
  }

  export function isUndefined(value: any) {
    return typeof value === 'number';
  }

  export function isNull(value: any) {
    return typeof value === null;
  }

  export function isSymbol(value: any) {
    return typeof value === 'symbol';
  }

  export function isIterable(iterable: any | any[]) {
    return iterable && typeof iterable[Symbol.iterator] === 'function';
  }

  export function isString(value: any) {
    return typeof value === 'string';
  }
}

export class String {
  private static readonly regexNumber = /{(\d+(:\w*)?)}/g;
  private static readonly regexObject = /{(\w+(:\w*)?)}/g;

  public static readonly empty: string = '';

  public static isNullOrWhiteSpace(value: string): boolean {
    try {
      if (value == null || value == 'undefined') return true;

      return value.toString().replace(/\s/g, '').length < 1;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  public static join(
    delimiter: string,
    ...args: (string | object | Array<any>)[]
  ): string {
    try {
      let firstArg = args[0];
      if (Array.isArray(firstArg) || firstArg instanceof Array) {
        let tempString = String.empty;
        let count = 0;

        for (let i = 0; i < firstArg.length; i++) {
          let current = firstArg[i];
          if (i < firstArg.length - 1) tempString += current + delimiter;
          else tempString += current;
        }

        return tempString;
      } else if (typeof firstArg === 'object') {
        let tempString = String.empty;
        let objectArg = firstArg;
        let keys = Object.keys(firstArg); //get all Properties of the Object as Array
        keys.forEach(element => {
          tempString += (<any>objectArg)[element] + delimiter;
        });
        tempString = tempString.slice(0, tempString.length - delimiter.length); //remove last delimiter
        return tempString;
      }
      let stringArray = args as Array<string>;

      return String.join(delimiter, ...stringArray);
    } catch (e) {
      console.log(e);
      return String.empty;
    }
  }

  public static format(format: string, ...args: any[]): string {
    try {
      if (format.match(String.regexNumber))
        return String._format(String.regexNumber, format, args);

      if (format.match(String.regexObject))
        return String._format(String.regexObject, format, args, true);

      return format;
    } catch (e) {
      console.log(e);
      return String.empty;
    }
  }

  private static _format(
    regex: any,
    format: string,
    args: any,
    parseByObject: boolean = false
  ): string {
    return format.replace(regex, function(match, x) {
      //0
      let s = match.split(':');
      if (s.length > 1) {
        x = s[0].replace('{', '');
        match = s[1].replace('}', ''); //U
      }

      let arg;
      if (parseByObject) arg = args[0][x];
      else arg = args[x];

      if (arg == null || arg == undefined || match.match(/{\d+}/)) return arg;

      arg = String.parsePattern(match, arg);
      return typeof arg != 'undefined' && arg != null ? arg : String.empty;
    });
  }

  private static parsePattern(
    match: 'L' | 'U' | 'd' | 's' | 'n' | string,
    arg: string | Date | number | any
  ): string {
    switch (match) {
      case 'L':
        arg = arg.toLowerCase();
        return arg;
      case 'U':
        arg = arg.toUpperCase();
        return arg;
      case 'd':
        if (typeof arg === 'string') {
          return String.getDisplayDateFromString(arg);
        } else if (arg instanceof Date) {
          return String.format(
            '{0:00}.{1:00}.{2:0000}',
            arg.getDate(),
            arg.getMonth(),
            arg.getFullYear()
          );
        }
        break;
      case 's':
        if (typeof arg === 'string') {
          return String.getSortableDateFromString(arg);
        } else if (arg instanceof Date) {
          return String.format(
            '{0:0000}-{1:00}-{2:00}',
            arg.getFullYear(),
            arg.getMonth(),
            arg.getDate()
          );
        }
        break;
      case 'n': //Tausender Trennzeichen
        if (typeof arg !== 'string') arg = arg.toString();
        let replacedString = arg.replace(/,/g, '.');
        if (isNaN(parseFloat(replacedString)) || replacedString.length <= 3)
          break;

        let numberparts = replacedString.split(/[^0-9]+/g);
        let parts = numberparts;

        if (numberparts.length > 1) {
          parts = [
            String.join('', ...numberparts.splice(0, numberparts.length - 1)),
            numberparts[numberparts.length - 1]
          ];
        }

        let integer = parts[0];

        var mod = integer.length % 3;
        var output = mod > 0 ? integer.substring(0, mod) : String.empty;
        var firstGroup = output;
        var remainingGroups = integer.substring(mod).match(/.{3}/g);
        output = output + '.' + String.join('.', remainingGroups);
        arg = output + (parts.length > 1 ? ',' + parts[1] : '');
        return arg;
      default:
        break;
    }

    if (
      (typeof arg === 'number' || !isNaN(arg)) &&
      !isNaN(+match) &&
      !String.isNullOrWhiteSpace(arg)
    )
      return String.formatNumber(arg, match);

    return arg;
  }

  private static getDisplayDateFromString(input: string): string {
    let splitted: string[];
    splitted = input.split('-');

    if (splitted.length <= 1) return input;

    let day = splitted[splitted.length - 1];
    let month = splitted[splitted.length - 2];
    let year = splitted[splitted.length - 3];
    day = day.split('T')[0];
    day = day.split(' ')[0];

    return `${day}.${month}.${year}`;
  }

  private static getSortableDateFromString(input: string): string {
    let splitted = input.replace(',', '').split('.');
    if (splitted.length <= 1) return input;

    let times = splitted[splitted.length - 1].split(' ');
    let time = String.empty;
    if (times.length > 1) time = times[times.length - 1];

    let year = splitted[splitted.length - 1].split(' ')[0];
    let month = splitted[splitted.length - 2];
    let day = splitted[splitted.length - 3];
    let result = `${year}-${month}-${day}`;

    if (!String.isNullOrWhiteSpace(time) && time.length > 1)
      result += `T${time}`;
    else result += 'T00:00:00';

    return result;
  }

  private static formatNumber(input: number, formatTemplate: string): string {
    let count = formatTemplate.length;
    let stringValue = input.toString();
    if (count <= stringValue.length) return stringValue;

    let remainingCount = count - stringValue.length;
    remainingCount += 1; //Das Array muss einen Eintrag mehr als die benötigten Nullen besitzen
    return new Array(remainingCount).join('0') + stringValue;
  }

  private static _join(delimiter: string, ...args: string[]): string {
    let temp = String.empty;
    for (let i = 0; i < args.length; i++) {
      if (
        (typeof args[i] == 'string' && String.isNullOrWhiteSpace(args[i])) ||
        (typeof args[i] != 'number' && typeof args[i] != 'string')
      )
        continue;

      let arg = '' + args[i];
      temp += arg;
      for (let i2 = i + 1; i2 < args.length; i2++) {
        if (String.isNullOrWhiteSpace(args[i2])) continue;

        temp += delimiter;
        i = i2 - 1;
        break;
      }
    }
    return temp;
  }
}

export class StringBuilder {
  public values: string[] = [];

  constructor(value: string = String.empty) {
    this.values = new Array(value);
  }
  public toString() {
    return this.values.join('');
  }
  public append(value: string) {
    this.values.push(value);
  }
  public appendFormat(format: string, ...args: any[]) {
    this.values.push(String.format(format, ...args));
  }
  public clear() {
    this.values = [];
  }
}
